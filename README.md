This is a small Ruby class to calculate the distance (in meters) between two geo points.

For reference:  http://en.wikipedia.org/wiki/Haversine_formula

Usage:

```
require File.join(File.dirname(__FILE__), "path", "to", "haversine")

Haversine.new.distance_in_meters(lat1, lon1, lat2, lon2) # 534.23433 meters
```