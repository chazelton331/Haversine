class Haversine
  EARTH_RADIUS = 6371.0 # in kilometers

  # http://en.wikipedia.org/wiki/Haversine_formula
  def distance_in_meters(point_latitude, point_longitude, center_latitude, center_longitude)

    degrees_latitude  = to_radians(center_latitude -  point_latitude )
    degrees_longitude = to_radians(center_longitude - point_longitude)

    a = Math.sin(degrees_latitude/2.0       ) *
        Math.sin(degrees_latitude/2.0       ) +
        Math.cos(to_radians(point_latitude) ) *
        Math.cos(to_radians(center_latitude)) *
        Math.sin(degrees_longitude/2.0      ) *
        Math.sin(degrees_longitude/2.0      )

    c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a))
    (EARTH_RADIUS * c) * 1000.0 # convert to meters
  end

  private

  def to_radians(degrees)
    degrees * (Math::PI/180.0)
  end

end
